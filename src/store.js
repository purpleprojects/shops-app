import Vue from 'vue'
import Vuex from 'vuex'
import Shops from './StoreModules/Shops'

//Here I'm loading Vuex
Vue.use(Vuex)

//this is the store of all the web-app
export default new Vuex.Store({
  modules : {
    Shops,
  },
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
