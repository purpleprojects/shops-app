import Axios from "axios";

const state = {
    shopsNearby: [],
    shopsPrefered: [],
    cordinations : {
        lat : localStorage.getItem('position_lat') || null,
        longt : localStorage.getItem('position_longt') || null,
    },
    credentials : null,
    token : localStorage.getItem('access_token') || null,
    loggedIn : null,
};


const getters = {
    allShopsNearby: (state) => state.shopsNearby,
    allShopsPrefered: (state) => state.shopsPrefered,
    allCredentials: (state) => state.credentials,
    allCordinations: (state) => state.cordinations,
    getLoggedIn: (state) => state.loggedIn,
};

const actions = {
    checkIfLoggedIn({ commit }){
        return new Promise((resolve, reject) => {
            let loggedIn = state.token !== null ? true : false;
            if(loggedIn === true){
                Axios.defaults.headers.common['authorization'] = state.token;
                Axios.post(process.env.VUE_APP_LINK_API+'/checkLoggedIn')
                                        .then(function (response) {
                                            resolve(response);
                                        })
                                        .catch(function (error) {
                                            loggedIn = false;
                                            reject(error);                              
                                        })
                                        .then(function () {
                                            
                                        });
            } 
            if(!loggedIn){localStorage.removeItem('access_token');reject(); state.loggedIn = false;}else { resolve(); state.loggedIn = true;}
            commit('IfLoggedIn', loggedIn)
        });
    },
    logoutApp({ commit }){
        return new Promise((resolve, reject) => {
            Axios.defaults.headers.common['authorization'] = state.token;
            Axios.post(process.env.VUE_APP_LINK_API+'/logout')
                                .then(function (response) {
                                    resolve(response);
                                })
                                .catch(function (error) {
                                    reject(error);
                                })
                                .then(function () {
                                    commit('destroyToken');
                                });});
        
    },

    loginUser({ commit }, credentials) {
        const emailLog = credentials.email;
        const passwordLog = credentials.password;

        return new Promise((resolve, reject) => {
            Axios.post(process.env.VUE_APP_LINK_API+'/login', { "email" : emailLog, "password" : passwordLog })
                                .then(function (response) {
                                    const access_token = response.headers.authorization;
                                    localStorage.setItem('access_token',access_token);
                                    commit('retrieveToken', access_token);
                                    resolve(response);
                                })
                                .catch(function (error) {
                                    reject(error);
                                })
                                .then(function () {
                                    
                                });});
 
    },

    registerUser( { commit },credentials) {
        console.log("ddd ="+credentials);

        const emailLog = credentials.email;
        const passwordLog = credentials.password;
        const rePasswordLog = credentials.rePassword;
        console.log("email ="+emailLog);
        console.log("password ="+passwordLog);
        console.log("repassword ="+rePasswordLog);

        return new Promise((resolve, reject) => {
            Axios.post(process.env.VUE_APP_LINK_API+'/register', { "email" : emailLog, "password" : passwordLog, "repassword" : rePasswordLog})
                                .then(function (response) {
                                    resolve(response);
                                })
                                .catch(function (error) {
                                    reject(error);
                                })
                                .then(function () {
                                    
                                });});
 
    },

    fetchPosition( { commit } ){
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
                var cords = {
                    lat : position.coords.latitude,
                    longt : position.coords.longitude
                };
                localStorage.setItem('position_lat',cords.lat);
                localStorage.setItem('position_longt',cords.longt);
                commit('setPosition', cords)  
            })
        }
    },

    async fetchShopsNearby( { commit } ){
        Axios.defaults.headers.common['authorization'] = state.token;
        var lat = ""+state.cordinations.lat;
        var longt = ""+state.cordinations.longt;
        const response = await Axios.get(process.env.VUE_APP_LINK_API+'/shops/near',{ params :{ "lat" : lat, "longt" : longt}});
        commit('setShopsNearby', response.data)  
    },

    async fetchShopsPrefered( { commit } ){
        Axios.defaults.headers.common['authorization'] = state.token;
        const response = await Axios.get(process.env.VUE_APP_LINK_API+'/shops/prefered'); 
        commit('setShopsPrefered', response.data )
    },

    async likeShop({ commit }, shop) {
        var idShop = shop.id;
        Axios.defaults.headers.common['authorization'] = state.token;
        await Axios.post(process.env.VUE_APP_LINK_API+'/shops/addPreferedShop', { "id" : idShop, completed: false});
        commit('favorShop', shop)
    },

    async dislikeShop({ commit }, idShop) {
        Axios.defaults.headers.common['authorization'] = state.token;
        await Axios.post(process.env.VUE_APP_LINK_API+'/shops/addDislikedShop', {  "id" : idShop, completed: false});
        commit('disfavorShop', idShop)
    },

    async deletePreferedShop({ commit }, shop){
        var idShop = shop.id;
        Axios.defaults.headers.common['authorization'] = state.token;
        await Axios.post(process.env.VUE_APP_LINK_API+'/shops/deletePreferedShop', { "id" : idShop, completed: false});
        commit('removePreferedShop', shop);
    },
};

const mutations = {
    IfLoggedIn: (state, loggedIn) =>{
        if(!loggedIn){state.token = null;}
    },

    destroyToken: (state) => {
        localStorage.removeItem('access_token');
        state.token = null;
        state.loggedIn = false;
    },

    retrieveToken: (state, access_token) => {
        state.token = access_token;
        state.loggedIn = true;
    },

    setShopsNearby: (state, shops) => (state.shopsNearby = shops),
    setShopsPrefered: (state, shops) => (state.shopsPrefered = shops),
    favorShop (state, shop) {state.shopsPrefered.unshift(shop);state.shopsNearby =  state.shopsNearby.filter(shopNearby => shopNearby !== shop)},
    disfavorShop: (state, id) => {state.shopsNearby = state.shopsNearby.filter(shopNearby => shopNearby.id !== id)},
    removePreferedShop: (state, shop) => {state.shopsNearby.unshift(shop); state.shopsPrefered = state.shopsPrefered.filter(shopPrefered => shopPrefered !== shop)},
    setPosition: (state, cords) => {state.cordinations = cords }
};

export default {
    state,

    getters,

    actions,

    mutations
}
