import Vue from 'vue'
import Router from 'vue-router'
import ShopsNearby from './views/ShopsNearby.vue'
import Registration from './views/Registration.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/shops-nearby',
      name: 'shops-nearby',
      component: ShopsNearby
    },
    {
      path: '/shops-prefered',
      name: 'shops-prefered',
      component: () => import(/* webpackChunkName: "about" */ './views/ShopsPrefered.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue')
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration
    }
  ]
})
